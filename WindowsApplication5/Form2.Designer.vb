﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_brgy = New System.Windows.Forms.TextBox()
        Me.Lbl_LU = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_LU = New System.Windows.Forms.TextBox()
        Me.txt_WV = New System.Windows.Forms.TextBox()
        Me.txt_PD = New System.Windows.Forms.TextBox()
        Me.txt_MC = New System.Windows.Forms.TextBox()
        Me.txt_E = New System.Windows.Forms.TextBox()
        Me.txt_DB = New System.Windows.Forms.TextBox()
        Me.txt_LT = New System.Windows.Forms.TextBox()
        Me.buy_LU = New System.Windows.Forms.Button()
        Me.buy_WV = New System.Windows.Forms.Button()
        Me.buy_PD = New System.Windows.Forms.Button()
        Me.buy_MC = New System.Windows.Forms.Button()
        Me.buy_E = New System.Windows.Forms.Button()
        Me.buy_DB = New System.Windows.Forms.Button()
        Me.buy_LT = New System.Windows.Forms.Button()
        Me.txt_details = New System.Windows.Forms.TextBox()
        Me.buy_brgy = New System.Windows.Forms.Button()
        Me.txt_inc = New System.Windows.Forms.TextBox()
        Me.txt_exp = New System.Windows.Forms.TextBox()
        Me.lbl_inc = New System.Windows.Forms.Label()
        Me.lbl_exp = New System.Windows.Forms.Label()
        Me.detail = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.sell_brgy = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txt_brgy
        '
        Me.txt_brgy.Enabled = False
        Me.txt_brgy.Location = New System.Drawing.Point(28, 24)
        Me.txt_brgy.Name = "txt_brgy"
        Me.txt_brgy.Size = New System.Drawing.Size(170, 20)
        Me.txt_brgy.TabIndex = 3
        '
        'Lbl_LU
        '
        Me.Lbl_LU.AutoSize = True
        Me.Lbl_LU.Location = New System.Drawing.Point(40, 206)
        Me.Lbl_LU.Name = "Lbl_LU"
        Me.Lbl_LU.Size = New System.Drawing.Size(53, 13)
        Me.Lbl_LU.TabIndex = 4
        Me.Lbl_LU.Text = "Land Use"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 230)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 13)
        Me.Label1.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 302)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Elevation"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(40, 278)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Distance to Main Channel"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(40, 254)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Population Density"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(40, 324)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Distance to the Bay"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(40, 349)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Lag Time"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(40, 230)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(76, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Water Velocity"
        '
        'txt_LU
        '
        Me.txt_LU.Enabled = False
        Me.txt_LU.Location = New System.Drawing.Point(237, 203)
        Me.txt_LU.Multiline = True
        Me.txt_LU.Name = "txt_LU"
        Me.txt_LU.ShortcutsEnabled = False
        Me.txt_LU.Size = New System.Drawing.Size(100, 20)
        Me.txt_LU.TabIndex = 12
        '
        'txt_WV
        '
        Me.txt_WV.Enabled = False
        Me.txt_WV.Location = New System.Drawing.Point(237, 227)
        Me.txt_WV.Name = "txt_WV"
        Me.txt_WV.Size = New System.Drawing.Size(100, 20)
        Me.txt_WV.TabIndex = 13
        '
        'txt_PD
        '
        Me.txt_PD.Enabled = False
        Me.txt_PD.Location = New System.Drawing.Point(237, 251)
        Me.txt_PD.Name = "txt_PD"
        Me.txt_PD.Size = New System.Drawing.Size(100, 20)
        Me.txt_PD.TabIndex = 14
        '
        'txt_MC
        '
        Me.txt_MC.Enabled = False
        Me.txt_MC.Location = New System.Drawing.Point(237, 275)
        Me.txt_MC.Name = "txt_MC"
        Me.txt_MC.Size = New System.Drawing.Size(100, 20)
        Me.txt_MC.TabIndex = 15
        '
        'txt_E
        '
        Me.txt_E.Enabled = False
        Me.txt_E.Location = New System.Drawing.Point(237, 299)
        Me.txt_E.Name = "txt_E"
        Me.txt_E.Size = New System.Drawing.Size(100, 20)
        Me.txt_E.TabIndex = 16
        '
        'txt_DB
        '
        Me.txt_DB.Enabled = False
        Me.txt_DB.Location = New System.Drawing.Point(237, 325)
        Me.txt_DB.Name = "txt_DB"
        Me.txt_DB.Size = New System.Drawing.Size(100, 20)
        Me.txt_DB.TabIndex = 17
        '
        'txt_LT
        '
        Me.txt_LT.Enabled = False
        Me.txt_LT.Location = New System.Drawing.Point(237, 349)
        Me.txt_LT.Name = "txt_LT"
        Me.txt_LT.Size = New System.Drawing.Size(100, 20)
        Me.txt_LT.TabIndex = 18
        '
        'buy_LU
        '
        Me.buy_LU.Location = New System.Drawing.Point(354, 203)
        Me.buy_LU.Name = "buy_LU"
        Me.buy_LU.Size = New System.Drawing.Size(48, 21)
        Me.buy_LU.TabIndex = 19
        Me.buy_LU.Text = "BUY"
        Me.buy_LU.UseVisualStyleBackColor = True
        '
        'buy_WV
        '
        Me.buy_WV.Location = New System.Drawing.Point(354, 227)
        Me.buy_WV.Name = "buy_WV"
        Me.buy_WV.Size = New System.Drawing.Size(48, 20)
        Me.buy_WV.TabIndex = 20
        Me.buy_WV.Text = "BUY"
        Me.buy_WV.UseVisualStyleBackColor = True
        '
        'buy_PD
        '
        Me.buy_PD.Location = New System.Drawing.Point(354, 251)
        Me.buy_PD.Name = "buy_PD"
        Me.buy_PD.Size = New System.Drawing.Size(48, 21)
        Me.buy_PD.TabIndex = 21
        Me.buy_PD.Text = "BUY"
        Me.buy_PD.UseVisualStyleBackColor = True
        '
        'buy_MC
        '
        Me.buy_MC.Location = New System.Drawing.Point(354, 275)
        Me.buy_MC.Name = "buy_MC"
        Me.buy_MC.Size = New System.Drawing.Size(48, 21)
        Me.buy_MC.TabIndex = 22
        Me.buy_MC.Text = "BUY"
        Me.buy_MC.UseVisualStyleBackColor = True
        '
        'buy_E
        '
        Me.buy_E.Location = New System.Drawing.Point(354, 302)
        Me.buy_E.Name = "buy_E"
        Me.buy_E.Size = New System.Drawing.Size(48, 21)
        Me.buy_E.TabIndex = 23
        Me.buy_E.Text = "BUY"
        Me.buy_E.UseVisualStyleBackColor = True
        '
        'buy_DB
        '
        Me.buy_DB.Location = New System.Drawing.Point(354, 325)
        Me.buy_DB.Name = "buy_DB"
        Me.buy_DB.Size = New System.Drawing.Size(48, 21)
        Me.buy_DB.TabIndex = 24
        Me.buy_DB.Text = "BUY"
        Me.buy_DB.UseVisualStyleBackColor = True
        '
        'buy_LT
        '
        Me.buy_LT.Location = New System.Drawing.Point(354, 349)
        Me.buy_LT.Name = "buy_LT"
        Me.buy_LT.Size = New System.Drawing.Size(48, 21)
        Me.buy_LT.TabIndex = 25
        Me.buy_LT.Text = "BUY"
        Me.buy_LT.UseVisualStyleBackColor = True
        '
        'txt_details
        '
        Me.txt_details.Enabled = False
        Me.txt_details.Location = New System.Drawing.Point(237, 24)
        Me.txt_details.Multiline = True
        Me.txt_details.Name = "txt_details"
        Me.txt_details.Size = New System.Drawing.Size(202, 154)
        Me.txt_details.TabIndex = 26
        '
        'buy_brgy
        '
        Me.buy_brgy.Location = New System.Drawing.Point(28, 50)
        Me.buy_brgy.Name = "buy_brgy"
        Me.buy_brgy.Size = New System.Drawing.Size(170, 23)
        Me.buy_brgy.TabIndex = 27
        Me.buy_brgy.Text = "BUY BRGY  $500"
        Me.buy_brgy.UseVisualStyleBackColor = True
        '
        'txt_inc
        '
        Me.txt_inc.Enabled = False
        Me.txt_inc.Location = New System.Drawing.Point(91, 81)
        Me.txt_inc.Name = "txt_inc"
        Me.txt_inc.Size = New System.Drawing.Size(107, 20)
        Me.txt_inc.TabIndex = 28
        Me.txt_inc.Visible = False
        '
        'txt_exp
        '
        Me.txt_exp.Enabled = False
        Me.txt_exp.Location = New System.Drawing.Point(91, 107)
        Me.txt_exp.Name = "txt_exp"
        Me.txt_exp.Size = New System.Drawing.Size(107, 20)
        Me.txt_exp.TabIndex = 29
        Me.txt_exp.Visible = False
        '
        'lbl_inc
        '
        Me.lbl_inc.AutoSize = True
        Me.lbl_inc.Location = New System.Drawing.Point(28, 86)
        Me.lbl_inc.Name = "lbl_inc"
        Me.lbl_inc.Size = New System.Drawing.Size(42, 13)
        Me.lbl_inc.TabIndex = 30
        Me.lbl_inc.Text = "Income"
        Me.lbl_inc.Visible = False
        '
        'lbl_exp
        '
        Me.lbl_exp.AutoSize = True
        Me.lbl_exp.Location = New System.Drawing.Point(28, 113)
        Me.lbl_exp.Name = "lbl_exp"
        Me.lbl_exp.Size = New System.Drawing.Size(47, 13)
        Me.lbl_exp.TabIndex = 31
        Me.lbl_exp.Text = "expense"
        Me.lbl_exp.Visible = False
        '
        'detail
        '
        Me.detail.Location = New System.Drawing.Point(408, 203)
        Me.detail.Name = "detail"
        Me.detail.Size = New System.Drawing.Size(54, 21)
        Me.detail.TabIndex = 32
        Me.detail.Text = "DETAIL"
        Me.detail.UseVisualStyleBackColor = True
        Me.detail.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(237, 403)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 33
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(139, 406)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 13)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "Vulnerability Index"
        '
        'sell_brgy
        '
        Me.sell_brgy.Location = New System.Drawing.Point(28, 144)
        Me.sell_brgy.Name = "sell_brgy"
        Me.sell_brgy.Size = New System.Drawing.Size(170, 23)
        Me.sell_brgy.TabIndex = 35
        Me.sell_brgy.Text = "SELL $350"
        Me.sell_brgy.UseVisualStyleBackColor = True
        Me.sell_brgy.Visible = False
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 435)
        Me.Controls.Add(Me.sell_brgy)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.detail)
        Me.Controls.Add(Me.lbl_exp)
        Me.Controls.Add(Me.lbl_inc)
        Me.Controls.Add(Me.txt_exp)
        Me.Controls.Add(Me.txt_inc)
        Me.Controls.Add(Me.buy_brgy)
        Me.Controls.Add(Me.txt_details)
        Me.Controls.Add(Me.buy_LT)
        Me.Controls.Add(Me.buy_DB)
        Me.Controls.Add(Me.buy_E)
        Me.Controls.Add(Me.buy_MC)
        Me.Controls.Add(Me.buy_PD)
        Me.Controls.Add(Me.buy_WV)
        Me.Controls.Add(Me.buy_LU)
        Me.Controls.Add(Me.txt_LT)
        Me.Controls.Add(Me.txt_DB)
        Me.Controls.Add(Me.txt_E)
        Me.Controls.Add(Me.txt_MC)
        Me.Controls.Add(Me.txt_PD)
        Me.Controls.Add(Me.txt_WV)
        Me.Controls.Add(Me.txt_LU)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Lbl_LU)
        Me.Controls.Add(Me.txt_brgy)
        Me.MaximumSize = New System.Drawing.Size(500, 474)
        Me.MinimumSize = New System.Drawing.Size(500, 474)
        Me.Name = "Form2"
        Me.Text = "Brgy. Info"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_brgy As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_LU As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_LU As System.Windows.Forms.TextBox
    Friend WithEvents txt_WV As System.Windows.Forms.TextBox
    Friend WithEvents txt_PD As System.Windows.Forms.TextBox
    Friend WithEvents txt_MC As System.Windows.Forms.TextBox
    Friend WithEvents txt_E As System.Windows.Forms.TextBox
    Friend WithEvents txt_DB As System.Windows.Forms.TextBox
    Friend WithEvents txt_LT As System.Windows.Forms.TextBox
    Friend WithEvents buy_LU As System.Windows.Forms.Button
    Friend WithEvents buy_WV As System.Windows.Forms.Button
    Friend WithEvents buy_PD As System.Windows.Forms.Button
    Friend WithEvents buy_MC As System.Windows.Forms.Button
    Friend WithEvents buy_E As System.Windows.Forms.Button
    Friend WithEvents buy_DB As System.Windows.Forms.Button
    Friend WithEvents buy_LT As System.Windows.Forms.Button

    Private Sub Label8_Click(sender As Object, e As EventArgs)

    End Sub
    Friend WithEvents txt_details As System.Windows.Forms.TextBox

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txt_details.TextChanged

    End Sub

    Private Sub txt_brgy_TextChanged(sender As Object, e As EventArgs) Handles txt_brgy.TextChanged
        If txt_brgy.Text = "BRGY. 719" Then txt_details.Text = "Land Area: 600,031.90 sq.m  Price: $50/sqm."
        If txt_brgy.Text = "BRGY. 701" Then txt_details.Text = "Land Area: 194,705.96 sq.m  Price: $50/sqm."
    End Sub

    Private Sub buy_LU_Click(sender As Object, e As EventArgs) Handles buy_LU.Click
        If txt_brgy.Text = "BRGY. 719" Then txt_LU.Text = "CN = 91.16"
        If txt_brgy.Text = "BRGY. 701" Then txt_LU.Text = "CN = 89.73"
        detail.Visible = True
    End Sub

    Private Sub buy_MC_Click(sender As Object, e As EventArgs) Handles buy_MC.Click
        If txt_brgy.Text = "BRGY. 719" Then txt_MC.Text = "292.291m"
        If txt_brgy.Text = "BRGY. 701" Then txt_MC.Text = "241.318m"
    End Sub
    Friend WithEvents buy_brgy As System.Windows.Forms.Button

    Private Sub buy_brgy_Click(sender As Object, e As EventArgs) Handles buy_brgy.Click
        Dim BP As Double

        txt_inc.Visible = True
        txt_exp.Visible = True
        lbl_inc.Visible = True
        lbl_exp.Visible = True
        If Form5.CheckBox2.Checked = True Then MessageBox.Show("already bought")
        BP = CDbl(Form5.TextBox1.Text) - 500
        Form5.TextBox1.Text = BP
        Form5.CheckBox2.Checked = True
        sell_brgy.Visible = True




    End Sub
    Friend WithEvents txt_inc As System.Windows.Forms.TextBox

    Private Sub TextBox1_TextChanged_1(sender As Object, e As EventArgs) Handles txt_inc.TextChanged

    End Sub
    Friend WithEvents txt_exp As System.Windows.Forms.TextBox

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles txt_exp.TextChanged

    End Sub
    Friend WithEvents lbl_inc As System.Windows.Forms.Label
    Friend WithEvents lbl_exp As System.Windows.Forms.Label

    Private Sub buy_PD_Click(sender As Object, e As EventArgs) Handles buy_PD.Click
        If txt_brgy.Text = "BRGY. 719" Then txt_PD.Text = "0.005568037"
        If txt_brgy.Text = "BRGY. 701" Then txt_PD.Text = "0.002912083"

    End Sub

    Private Sub buy_E_Click(sender As Object, e As EventArgs) Handles buy_E.Click
        If txt_brgy.Text = "BRGY. 719" Then txt_E.Text = "10m"
        If txt_brgy.Text = "BRGY. 701" Then txt_E.Text = "11m"

    End Sub

    Private Sub buy_DB_Click(sender As Object, e As EventArgs) Handles buy_DB.Click
        If txt_brgy.Text = "BRGY. 719" Then txt_DB.Text = "538.785m"
        If txt_brgy.Text = "BRGY. 701" Then txt_DB.Text = "250.052m"
    End Sub

    Private Sub buy_LT_Click(sender As Object, e As EventArgs) Handles buy_LT.Click
        If txt_brgy.Text = "BRGY. 719" Then txt_LT.Text = "470.5091 min"
        If txt_brgy.Text = "BRGY. 701" Then txt_LT.Text = "516.5076 min"

    End Sub
    Friend WithEvents detail As System.Windows.Forms.Button

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles detail.Click
        Form3.Show()
        If Form3.txt_brgy.Text = "BRGY. 719" Then Form3.txt_com.Text = "583,750.03 sq.m"
        If Form3.txt_brgy.Text = "BRGY. 719" Then Form3.txt_res.Text = "0 sq.m"
        If Form3.txt_brgy.Text = "BRGY. 719" Then Form3.txt_ind.Text = "0 sq.m"
        If Form3.txt_brgy.Text = "BRGY. 719" Then Form3.txt_open.Text = "16,281.87 sq.m"

        If Form3.txt_brgy.Text = "BRGY. 701" Then Form3.txt_com.Text = "131,569.90 sq.m "
        If Form3.txt_brgy.Text = "BRGY. 701" Then Form3.txt_res.Text = "63,136.06 sq.m"
        If Form3.txt_brgy.Text = "BRGY. 701" Then Form3.txt_ind.Text = "0 sq.m"
        If Form3.txt_brgy.Text = "BRGY. 701" Then Form3.txt_open.Text = "0 sq.m"

    End Sub

    Private Sub buy_WV_Click(sender As Object, e As EventArgs) Handles buy_WV.Click
        If txt_brgy.Text = "BRGY. 719" Then txt_WV.Text = "0.74600"
        If txt_brgy.Text = "BRGY. 701" Then txt_WV.Text = "0.77757"
    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox

    Private Sub TextBox1_TextChanged_2(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

    End Sub
    Friend WithEvents Label8 As System.Windows.Forms.Label

    Private Sub Label8_Click_1(sender As Object, e As EventArgs) Handles Label8.Click

    End Sub
    Friend WithEvents sell_brgy As System.Windows.Forms.Button

    Private Sub sell_brgy_Click(sender As Object, e As EventArgs) Handles sell_brgy.Click
        Dim BP As Double
        BP = CDbl(Form5.TextBox1.Text) + 350
        Form5.TextBox1.Text = BP
        sell_brgy.Visible = False
        Form5.CheckBox2.Checked = False

    End Sub
End Class
